from opentrons import protocol_api

# metadata
metadata = {
    'protocolName': '2 fold serial dilution of alpha factor',
    'author': 'Fergus Harrison Fergus.harrison@hdr.mq.edu.au',
    'description': '2-fold serial dilution of alpha factor from 1x to 1/1634x. Done 4 times for different strains',
    'apiLevel': '2.0'
}

useable_wells = {
    1: 'B2',
    2: 'B3',
    3: 'B4',
    4: 'B5',
    5: 'B6',
    6: 'B7',
    7: 'B8',
    8: 'B9',
    9: 'B10',
    10:'B11',
    11:'C2',
    12:'C3',
    13:'C4',
    14:'C5',
    15:'C6',
    16:'C7',
    17:'C8',
    18:'C9',
    19:'C10',
    20:'C11',
    21:'D2',
    22:'D3',
    23:'D4',
    24:'D5',
    25:'D6',
    26:'D7',
    27:'D8',
    28:'D9',
    29:'D10',
    30:'D11',
    31:'E2',
    32:'E3',
    33:'E4',
    34:'E5',
    35:'E6',
    36:'E7',
    37:'E8',
    38:'E9',
    39:'E10',
    40:'E11',
    41:'F2',
    42:'F3',
    43:'F4',
    44:'F5',
    45:'F6',
    46:'F7',
    47:'F8',
    48:'F9',
    49:'F10',
    50:'F11',
    51:'G2',
    52:'G3',
    53:'G4',
    54:'G5',
    55:'G6',
    56:'G7',
    57:'G8',
    58:'G9',
    59:'G10',
    60:'G11',
    }
# protocol run function. the part after the colon lets your editor know
# where to look for autocomplete suggestions
def run(protocol: protocol_api.ProtocolContext):

    # labware
    plate = protocol.load_labware('corning_96_wellplate_360ul_flat', '2')
    tiprack = protocol.load_labware('opentrons_96_tiprack_300ul', '1')
    tube_rack = protocol.load_labware('opentrons_24_tuberack_eppendorf_2ml_safelock_snapcap', '6')

    # pipettes
    left_pipette = protocol.load_instrument('p300_multi', 'left', tip_racks=[tiprack])
    right_pipette = protocol.load_instrument('p50_single', 'right', tip_racks=[tiprack])
    

    # commands
    # Negative control setup
    offset=0
    right_pipette.pick_up_tip()
    right_pipette.transfer(150.0, tube_rack['A1'], plate['B2'], mix_after=(4, 50), new_tip='never')
    for i in range(2,16):
        n = i+offset
        prev_well = plate[useable_wells[n-1]]
        next_well = plate[useable_wells[n]]
        right_pipette.transfer(150.0, prev_well, next_well, mix_after=(4, 50), new_tip='never')
    right_pipette.drop_tip()
    
    # Positive control setup
    offset = offset+15
    right_pipette.pick_up_tip()
    right_pipette.transfer(150.0, tube_rack['A1'], plate['C7'], mix_after=(4, 50), new_tip='never')
    for i in range(2,16):
        n = i+offset
        prev_well = plate[useable_wells[n-1]]
        next_well = plate[useable_wells[n]]
        right_pipette.transfer(150.0, prev_well, next_well, mix_after=(4, 50), new_tip='never')
    right_pipette.drop_tip()

    # Strain 1 setup
    offset = offset+15
    right_pipette.pick_up_tip()
    right_pipette.transfer(150.0, tube_rack['A1'], plate['E2'], mix_after=(4, 50), new_tip='never')
    for i in range(2,16):
        n = i+offset
        prev_well = plate[useable_wells[n-1]]
        next_well = plate[useable_wells[n]]
        right_pipette.transfer(150.0, prev_well, next_well, mix_after=(4, 50), new_tip='never')
    right_pipette.drop_tip()
    
    # Strain 2 setup
    offset = offset+15
    right_pipette.pick_up_tip()
    right_pipette.transfer(150.0, tube_rack['A1'], plate['F7'], mix_after=(4, 50), new_tip='never')
    for i in range(2,16):
        n = i+offset
        prev_well = plate[useable_wells[n-1]]
        next_well = plate[useable_wells[n]]
        right_pipette.transfer(150.0, prev_well, next_well, mix_after=(4, 50), new_tip='never')
    right_pipette.drop_tip()