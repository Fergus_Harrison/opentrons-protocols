from opentrons import protocol_api

# metadata
metadata = {
    'protocolName': 'Distribute 150ul',
    'author': 'Fergus Harrison Fergus.harrison@hdr.mq.edu.au',
    'description': 'Simple distribution protocol used to divy out 150ul of water to 96 wells',
    'apiLevel': '2.0'
}

# protocol run function. the part after the colon lets your editor know
# where to look for autocomplete suggestions
def run(protocol: protocol_api.ProtocolContext):

    # labware
    plate = protocol.load_labware('corning_96_wellplate_360ul_flat', '2')
    tiprack = protocol.load_labware('opentrons_96_tiprack_300ul', '1')
    trough = protocol.load_labware('MQ_4_well_reservoir_70000ul', '6', namespace='custom_beta')

    # pipettes
    left_pipette = protocol.load_instrument('p300_multi', 'left', tip_racks=[tiprack])
    
    # commands
    left_pipette.distribute(150.0, trough['A1'], plate.columns())
