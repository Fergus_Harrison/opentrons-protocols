import opentrons
from opentrons import robot, instruments, labware


metadata = {
    'protocolName': 'Distribute taq',
    'author': 'Fergus Harrison',
    'description': 'Protocol to distribute 14ul of a taq mastermix, 1ul of the template DNA is expected to up added by the Labcyte Echo550'
    }

tiprack = labware.load('opentrons_96_tiprack_300ul', '1', 'p300rack')
PCR_384 = labware.load('corning_384_wellplate_112ul_flat', '3','zymolyaseYeast_plate')
tube_rack = labware.load('opentrons_24_tuberack_eppendorf_2ml_safelock_snapcap', '6','trough')
pipette_50 = instruments.P50_Single(mount='right',tip_racks=[tiprack])

robot.comment('Distributing PCR mastermix from tubes to 384 well plate.')
# Fill the zymolyase plate from the eppy tubes 
pipette_50.distribute(14, tube_rack.wells('A1', 'A2'), PCR_384.cols()[0:12], 
                      new_tip='once',
                      trash=True,
                      disposal_vol=0
                      )

robot.comment('Distribute accomplished. Use touchdown PCR to amplify fragments.')

