import opentrons
from opentrons import robot, instruments, labware
from opentrons.legacy_api.containers import placeable


metadata = {
    'protocolName': 'Yeast to Zymolase transfer',
    'author': 'Fergus Harrison',
    'description': '''Protocol used to fill half a 384 well plate with 11ul a 1U/ul conc soln of 
                      zymolyase and then fill each well with 30ul of different colonies from two
                      96-well PCR plates''',
    }

def wellpateTransfer_96_to_384(start_column, stop_column, wellplate_384):
    '''takes a given start and stop column in a 384 well plate and returns the wells that 
       corresponds to the tip positions of an 8-channel pipette for each column in a whole 
       96 well plate. This allows for direct 96well transfer to 384well
       This is not a robust method and will only work if the number of wells in 96well 
       cols used match the amount of columns to be used in the 384well plate''' 
    odd_96well_columns = []
    even_96well_columns = []
    for i in wellplate_384.cols()[start_column:stop_column]:
        odd_96well_columns = odd_96well_columns + [placeable.WellSeries(i[::2])]
        even_96well_columns = even_96well_columns + [placeable.WellSeries(i[1::2])]
    odd_wells = placeable.WellSeries(odd_96well_columns)
    even_wells = placeable.WellSeries(even_96well_columns)
    return odd_wells, even_wells

zym_plate = labware.load('corning_384_wellplate_112ul_flat', '3','zymolyase_plate')
growth_plate1 = labware.load('biorad_96_wellplate_200ul_pcr', '2', 'growth_plate')
growth_plate2 = labware.load('biorad_96_wellplate_200ul_pcr', '6', 'growth_plate')
eppy_rack = labware.load('opentrons_24_tuberack_eppendorf_2ml_safelock_snapcap_acrylic', '9','eppy_rack')

tiprack1 = labware.load('opentrons_96_tiprack_300ul', '1', 'p300rack')
tiprack2 = labware.load('opentrons_96_tiprack_300ul', '4', 'p300rack1')
tiprack3 = labware.load('opentrons_96_tiprack_300ul', '5', 'p300rack2')

pipette_50 = instruments.P50_Single(mount='right',tip_racks=[tiprack1])
pipette_300 = instruments.P300_Multi(mount='left',tip_racks=[tiprack2, tiprack3])

robot.comment('Distributing zymolase solution from eppy_rack to 96 well plate.')
# Fill the zymolyase plate from the eppy_rack
pipette_50.distribute(11, eppy_rack.wells('A1', 'A2'), zym_plate.cols()[0:12], 
                      disposal_vol=5,
                      new_tip='once',
                      trash=True,
                      disposal_volume=0
                      )

robot.comment('Transferring yeast colonies from growth plate to zymolase solution.')

# transfer growth colonies from growth plate to zymolyase plate
odd_wells, even_wells = wellpateTransfer_96_to_384(0, 6, zym_plate)

pipette_300.transfer(30, growth_plate1.cols()[::2], odd_wells, 
                      new_tip='always',
                      trash=True,
                      mix_before=(3, 30))
pipette_300.transfer(30, growth_plate1.cols()[1::2], even_wells, 
                      new_tip='always',
                      trash=True,
                      mix_before=(3, 30))

# Second plate transfer
odd_wells, even_wells = wellpateTransfer_96_to_384(6, 12, zym_plate)

pipette_300.transfer(30, growth_plate2.cols()[::2], odd_wells, 
                      new_tip='always',
                      trash=True,
                      mix_before=(3, 30))
pipette_300.transfer(30, growth_plate2.cols()[1::2], even_wells, 
                      new_tip='always',
                      trash=True,
                      mix_before=(3, 30))


robot.comment('Transfer accomplished. Leave colonies in zymolase for 50 mintues. then put in -80C')

