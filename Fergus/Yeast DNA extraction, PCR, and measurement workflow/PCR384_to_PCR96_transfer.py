import opentrons
from opentrons import robot, instruments, labware
from opentrons.legacy_api.containers import placeable


metadata = {
    'protocolName': 'Yeast to Zymolase transfer',
    'author': 'Fergus Harrison',
    'description': '''Protocol used to dispense 195ul of MQ water into 2 96well plates, 
                      and split a 384well(w\ 192 wells used) plate into the two 96well plates''',
    }

def wellpateTransfer_96_to_384(start_column, stop_column, wellplate_384):
    '''takes a given start and stop column in a 384 well plate and returns the wells that 
       corresponds to the tip positions of an 8-channel pipette for each column in a whole 
       96 well plate. This allows for direct 96well transfer to 384well
       This is not a robust method and will only work if the number of wells in 96well 
       cols used match the amount of columns to be used in the 384well plate''' 
    odd_96well_columns = []
    even_96well_columns = []
    for i in wellplate_384.cols()[start_column:stop_column]:
        odd_96well_columns = odd_96well_columns + [placeable.WellSeries(i[::2])]
        even_96well_columns = even_96well_columns + [placeable.WellSeries(i[1::2])]
    odd_wells = placeable.WellSeries(odd_96well_columns)
    even_wells = placeable.WellSeries(even_96well_columns)
    return odd_wells, even_wells


PCR96_plate1 = labware.load('adapter_96_wellplate_250ul_pcr', '2', 'PCR_plate1')
PCR96_plate2 = labware.load('adapter_96_wellplate_250ul_pcr', '3', 'PCR_plate2')
PCR384_plate = labware.load('384_PCRplate_Agilent', '6','PCR384')
water_trough = labware.load('Trough_4_well', '9', '4_trough')

tiprack1 = labware.load('opentrons_96_tiprack_300ul', '1', 'p300rack1')
tiprack2 = labware.load('opentrons_96_tiprack_300ul', '4', 'p300rack2')
tiprack3 = labware.load('opentrons_96_tiprack_300ul', '5', 'p300rack3')

pipette_50 = instruments.P50_Single(mount='right', tip_racks=[tiprack1, tiprack2])
pipette_300 = instruments.P300_Multi(mount='left', tip_racks=[tiprack3])

# Dispense 195ul of MQ H2O into each well of the 96well PCR plates.
robot.comment('Distributing MQ water.')

# Distribute MQ water to each well in two 96 well plates
pipette_300.pick_up_tip(tiprack3.cols('1'))
pipette_300.distribute(195, water_trough.wells('A1'), PCR96_plate1.cols(),
                        new_tip='never',
                        trash=True)
pipette_300.distribute(195, water_trough.wells('A1'), PCR96_plate2.cols(),
                        new_tip='never',
                        trash=True)
pipette_300.drop_tip()

#De-interlace the 384 wells back into two 96well plates\
robot.comment('De-interlacing 384well plate into 2 96well plates')

odd_wells, even_wells = wellpateTransfer_96_to_384(0, 6, PCR384_plate)
pipette_50.transfer(5, odd_wells, PCR96_plate1.cols()[::2], 
                      new_tip='always',
                      trash=True,
                      mix_before=(3, 10))
pipette_50.transfer(5, even_wells, PCR96_plate1.cols()[1::2], 
                      new_tip='always',
                      trash=True,
                      mix_before=(3, 10))

odd_wells, even_wells = wellpateTransfer_96_to_384(6, 12, PCR384_plate)
pipette_50.transfer(5, odd_wells, PCR96_plate2.cols()[::2], 
                      new_tip='always',
                      trash=True,
                      mix_before=(3, 10))
pipette_50.transfer(5, even_wells, PCR96_plate2.cols()[1::2], 
                      new_tip='always',
                      trash=True,
                      mix_before=(3, 10))