from opentrons import protocol_api

# metadata
metadata = {
    'protocolName': 'Distribute 124ul',
    'author': 'Fergus Harrison Fergus.harrison@hdr.mq.edu.au',
    'description': 'Simple distribution protocol used to divy out 9.1ul of water to 93 of 96 wells. It leaves the last well for ZAG ladder.',
    'apiLevel': '2.0'
}

# protocol run function. the part after the colon lets your editor know
# where to look for autocomplete suggestions
def run(protocol: protocol_api.ProtocolContext):

    # labware
    plate = protocol.load_labware('biorad_96_wellplate_200ul_pcr', '2')
    tiprack = protocol.load_labware('opentrons_96_tiprack_300ul', '1')
    # tube_rack = protocol.load_labware('opentrons_24_tuberack_eppendorf_2ml_safelock_snapcap', '6')
    tubes = protocol.load_labware('opentrons_24_tuberack_eppendorf_1.5ml_safelock_snapcap', '6')
    # mq_4_well_reservoir_70000ul

    # pipettes
    left_pipette = protocol.load_instrument('p300_multi', 'left', tip_racks=[tiprack])
    right_pipette = protocol.load_instrument('p50_single', 'right', tip_racks=[tiprack])
    

    # commands
    right_pipette.distribute(9.1, tubes['A1'], plate.rows()[:-1], newtip='once')
    right_pipette.distribute(9.1, tubes['A1'], plate.rows()[-1][:-3], newtip='once')