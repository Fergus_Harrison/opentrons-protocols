from opentrons import protocol_api

# metadata
metadata = {
    'protocolName': 'Distribute 124ul',
    'author': 'Fergus Harrison Fergus.harrison@hdr.mq.edu.au',
    'description': 'Simple distribution protocol used to divy out 124ul of water to 95 of 96 wells. It leaves the last well for ZAG ladder.',
    'apiLevel': '2.0'
}

# protocol run function. the part after the colon lets your editor know
# where to look for autocomplete suggestions
def run(protocol: protocol_api.ProtocolContext):

    # labware
    plate = protocol.load_labware('corning_96_wellplate_360ul_flat', '2')
    tiprack = protocol.load_labware('opentrons_96_tiprack_300ul', '1')
    # tube_rack = protocol.load_labware('opentrons_24_tuberack_eppendorf_2ml_safelock_snapcap', '6')
    trough = protocol.load_labware('MQ_4_well_reservoir_70000ul', '6', namespace='custom_beta')
    # mq_4_well_reservoir_70000ul

    # pipettes
    left_pipette = protocol.load_instrument('p300_multi', 'left', tip_racks=[tiprack])
    right_pipette = protocol.load_instrument('p50_single', 'right', tip_racks=[tiprack])
    

    # commands
    left_pipette.distribute(124.0, trough['A1'], plate.columns()[:-1])
    right_pipette.distribute(124.0, trough['A1'], plate['A12', 'B12', 'C12', 'D12', 'E12', 'F12', 'G12'])