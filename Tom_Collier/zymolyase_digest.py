import opentrons
from opentrons import robot, instruments, labware


metadata = {
    'protocolName': 'Yeast to Zymolase transfer',
    'author': 'Fergus Harrison',
    'description': '''Protocol used to take 70ul froma 384well echo plate and refill the wells with zymolyase mastermix''',
    }

Eppy_rack = labware.load('opentrons_24_aluminumblock_generic_2ml_screwcap', '6', 'eppy_rack')
ECHO384_plate = labware.load('corning_384_wellplate_112ul_flat', '3','ECHO384')

tiprack1 = labware.load('opentrons_96_tiprack_300ul', '1', 'p300rack1')
tiprack2 = labware.load('opentrons_96_tiprack_300ul', '2', 'p300rack2')

pipette_50 = instruments.P50_Single(mount='right', tip_racks=[tiprack1, tiprack2])
# pipette_300 = instruments.P300_Multi(mount='left', tip_racks=[tiprack2])

trash = labware.load('Trough_4_well', 4, 'trash')

# MOVE LIQUID TO TRASH
pipette_50.transfer(10, ECHO384_plate.rows('A')[0:23], trash.wells('A1'),
                      new_tip='always',
                      trash=True,
                      )
pipette_50.transfer(10, ECHO384_plate.rows('C')[0:16], trash.wells('A1'),
                      new_tip='always',
                      trash=True,)
pipette_50.transfer(10, ECHO384_plate.rows('E')[0:12], trash.wells('A1'),
                      new_tip='always',
                      trash=True,)

pipette_50.transfer(10, ECHO384_plate.rows('G')[0:23], trash.wells('A1'),
                      new_tip='always',
                      trash=True,)

# NOW MOVE ZYMOLYASE TO 384
pipette_50.transfer(10, Eppy_rack.wells('A1'), ECHO384_plate.rows('A')[0:23], 
                      new_tip='always',
                      trash=True,
                      disposal_volume=0
                      )
pipette_50.transfer(10, Eppy_rack.wells('A1'), ECHO384_plate.rows('C')[0:16], 
                      new_tip='always',
                      trash=True,
                      disposal_volume=0
                      )
pipette_50.transfer(10, Eppy_rack.wells('A1'), ECHO384_plate.rows('E')[0:12], 
                      new_tip='always',
                      trash=True,
                      disposal_volume=0
                      )
pipette_50.transfer(10, Eppy_rack.wells('A1'), ECHO384_plate.rows('G')[0:23], 
                      new_tip='always',
                      trash=True,
                      disposal_volume=0
                      )