import opentrons
from opentrons import robot, instruments, labware
from opentrons.legacy_api.containers import placeable


metadata = {
    'protocolName': 'Yeast to Zymolase transfer',
    'author': 'Fergus Harrison',
    'description': '''Defines a custom labware of the four column trough.''',
    }

# register trough in the robot (should only be needed the first time, but always good to have),
# Technically, the robot thinks this is a tray with circular wells.

create_trough = labware.create(
       'Trough_4_well',  # name of you labware
       grid=(4, 1),      # specify amount of (columns, rows)
       spacing=(1, 1),  # distances (mm) between each (column, row)
       diameter=25,      # diameter (mm) of each well on the plate
       depth=42,         # depth (mm) of each well on the plate
       volume=70000)
      ## # TODO adjust for real measurments