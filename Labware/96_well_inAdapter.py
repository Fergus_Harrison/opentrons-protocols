import opentrons
from opentrons import robot, instruments, labware
from opentrons.legacy_api.containers import placeable


metadata = {
    'protocolName': 'Yeast to Zymolase transfer',
    'author': 'Fergus Harrison',
    'description': '''Defines a custom labware of a semiskirted PCR 96well plate sitting in a fully skirted adapter, allowing it to sit in the OT-2.
    While the parameters given shouldn't be any different from a normal 96well plate, having a separate labware will allow the OT2 to remember the vertical height.
    This should make calibration easier.
    Adapter is made from a Bio-rad hard-shell 96 PCR wellplate with the wells drilled out.''',
    }

# register trough in the robot (should only be needed the first time, but always good to have),
# Technically, the robot thinks this is a tray with circular wells.

create_trough = labware.create(
       'adapter_96_wellplate_250ul_pcr',  # name of you labware
       grid=(12, 8),      # specify amount of (columns, rows)
       spacing=(9, 9),  # distances (mm) between each (column, row)
       diameter=5.46,      # diameter (mm) of each well on the plate
       depth=14.8,         # depth (mm) of each well on the plate
       volume=250)
      ## # TODO adjust for real measurments