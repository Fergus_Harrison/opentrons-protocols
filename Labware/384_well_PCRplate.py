import opentrons
from opentrons import robot, instruments, labware
from opentrons.legacy_api.containers import placeable


metadata = {
    'protocolName': 'Yeast to Zymolase transfer',
    'author': 'Fergus Harrison',
    'description': '''Defines a custom labware 384-PCR plate''',
    }

# register trough in the robot (should only be needed the first time, but always good to have),
# Technically, the robot thinks this is a tray with circular wells.

create_trough = labware.create(
       '384_PCRplate_Agilent',  # name of you labware
       grid = (24, 16),         # specify amount of (columns, rows)
       spacing = (4.5, 4.5),        # distances (mm) between each (column, row)
       diameter = 3.1,          # diameter (mm) of each well on the plate
       depth = 9.3,            # depth (mm) of each well on the plate
       volume = 25)             # (Optional) Maximum volume (ul) of a well, I think this is different to operational volume
