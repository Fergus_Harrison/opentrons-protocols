import opentrons
from opentrons import robot, instruments, labware
from opentrons.legacy_api.containers import placeable


metadata = {
    'protocolName': 'Yeast to Zymolase transfer',
    'author': 'Fergus Harrison',
    'description': '''Defines a custom labware made of the four column trough with an 1.5ml 
                      eppendorf tube taped to the back right corner of each trough. 
                      This has been superceded by official Opentrons labware''',
    }

# register trough in the robot (should only be needed the first time, but always good to have),
# Technically, the robot thinks this is a tray with circular wells.

create_trough = labware.create(
       'eppy_trough4',  # name of you labware
       grid=(4, 1),      # specify amount of (columns, rows)
       spacing=(28, 1),  # distances (mm) between each (column, row)
       diameter=10,      # diameter (mm) of each well on the plate
       depth=38,         # depth (mm) of each well on the plate
       volume=1500)
      ## # TODO make 2ml eppy